package xyz.keahie.clientuser;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

public class ClientUser extends Application {

    private static final String VERSION = "0.6.1";

    private Stage primaryStage;
    private ClientUserConnection clientUserConnection;
    private ClientUserController clientUserController;

    private int port;
    private String ip;

    private String username;
    private String protocolNumber;

    public ClientUser() throws IOException {
        this.port = 2903;
        //this.port = FileUtils.getInteger(new File("config.txt"), "port");
        this.ip = "localhost";
        //this.ip = "130.211.185.6";

        this.protocolNumber = "28d04w18y";
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setOnHiding(event -> {
            try {
                stop();
                System.exit(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        initGUI();
    }

    private void initGUI() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/xyz/keahie/clientuser/view/ChatClientInterface.fxml"));
        AnchorPane anchorPane = fxmlLoader.load();
        Scene scene = new Scene(anchorPane);

        clientUserController = fxmlLoader.getController();
        clientUserController.setClientUser(this);

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Chat Client Version " + VERSION);
        primaryStage.show();
    }

    public boolean connectToServer() {
        Socket socket;
        try {
            socket = new Socket(ip, port);
            System.out.println("Starting Client on port " + port);
            clientUserConnection = new ClientUserConnection(socket, this);
            clientUserConnection.start();
        } catch (ConnectException e) {
            System.out.println("Please be sure, that a server is running on " + ip + ":" + port);

            clientUserController.serverDisconnected();
            return false;
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public void stop() throws InterruptedException {
        Thread.sleep(100);
        clientUserController.disconnect(true, true);
    }

    public static void main(String[] args) {
        launch(args);
    }

    public ClientUserController getClientUserController() {
        return clientUserController;
    }

    public ClientUserConnection getClientUserConnection() {
        return clientUserConnection;
    }

    public int getPort() {
        return port;
    }

    public String getIp() {
        return ip;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public String getProtocolNumber() {
        return protocolNumber;
    }
}
