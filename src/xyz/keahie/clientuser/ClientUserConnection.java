package xyz.keahie.clientuser;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientUserConnection extends Thread {

    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;

    private Socket socket;
    private ClientUser clientUser;

    public ClientUserConnection(Socket socket, ClientUser clientUser) {
        this.socket = socket;
        this.clientUser = clientUser;
    }

    public void sendMessage(String message) throws IOException {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        dataOutputStream.writeUTF(message);
        dataOutputStream.flush();
    }

    @Override
    public void run() {
        try {
            setup(socket);
            sendMessage("sendprotocol " + clientUser.getProtocolNumber());
            while (true) {
                try {
                    while (!(dataInputStream.available() == 0)) {
                        Thread.sleep(1);
                    }
                    String messageFromServer = dataInputStream.readUTF();
                    if (messageFromServer.toLowerCase().startsWith("[~checking~]")) {
                        String[] args = messageFromServer.split(" ");
                        if (args[1].equals("true")) {
                            clientUser.getClientUserController().printMessage("You have the right version!");
                            break;
                        } else {
                            clientUser.getClientUserController().disconnect(false, true);
                            clientUser.getClientUserController().printMessage("You have the wrong version!");
                            close();
                            return;
                        }
                    }
                } catch (Exception e) {
                    break;
                }
            }
            sendMessage("getallrooms");
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                while (!(dataInputStream.available() == 0)) {
                    Thread.sleep(1);
                }
                String messageFromServer = dataInputStream.readUTF();
                if (messageFromServer.toLowerCase().startsWith("[~client~]")) {
                    String[] args = messageFromServer.split(" ");
                    switch (args[1].toLowerCase()) {
                        case "newroom":
                            clientUser.getClientUserController().addRoomToList(args[2]);
                            continue;
                        case "roomjoined":
                            clientUser.getClientUserController().printMessage("Joined room: " + args[2] + "\n");
                            setup(new Socket(socket.getInetAddress(), Integer.parseInt(args[3])));
                            clientUser.getClientUserController().roomConnected();
                            break;
                        case "roomleft":
                            clientUser.getClientUserController().printMessage("Left room: " + args[2] + "\n");
                            if (args[3].equals("false")) {
                                setup(new Socket(clientUser.getIp(), clientUser.getPort()));
                            } else {
                                socket = null;
                                clientUser.getClientUserController().disconnect(false, false);
                                break;
                            }
                            clientUser.getClientUserController().serverConnected();
                            break;
                    }
                    continue;
                }
                clientUser.getClientUserController().printMessage(messageFromServer);
                clientUser.getClientUserController().get

            } catch (Exception e) {
                break;
            }
        }
        try {
            close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setup(Socket socket) throws IOException {
        this.socket = socket;
        this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
        this.dataInputStream = new DataInputStream(socket.getInputStream());
    }

    private void close() throws IOException {
        if (dataInputStream != null && dataOutputStream != null) {
            dataOutputStream.close();
            dataInputStream.close();
        }
    }

    public Socket getSocket() {
        return socket;
    }
}
