package xyz.keahie.clientuser;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;

import java.awt.event.ActionEvent;
import java.io.IOException;

public class ClientUserController {

    @FXML
    private TextArea textArea;

    @FXML
    private TextField textField;

    @FXML
    private Button buttonSend;

    @FXML
    private Button buttonConnect;

    @FXML
    private Button buttonDisconnect;

    @FXML
    private MenuButton dropDownRoom;

    @FXML
    private Button buttonRoomDisconnect;

    private ClientUser clientUser;
    private boolean connected, inRoom;

    public ClientUserController() {
        this.textArea = new TextArea();
        this.textField = new TextField();
        this.buttonSend = new Button();
        this.buttonConnect = new Button();
        this.buttonDisconnect = new Button();
        this.dropDownRoom = new MenuButton();
        this.buttonRoomDisconnect = new Button();
    }

    @FXML
    private void initialize() {
        textField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                setUsername(textField.getText());
            }
        });
        buttonSend.setOnMouseClicked(event -> setUsername(textField.getText()));
        buttonConnect.setOnMouseClicked(event -> {
            textArea.setText("");
            printMessage("Trying to connect to the server \"" + clientUser.getIp() + ":" + clientUser.getPort() + "\"");
            if (clientUser.connectToServer()) {
                serverConnected();
                printMessage("Connected to the server\n");
            } else {
                printMessage("Can't connect to the server. Please be sure, that a server is running on " +
                        "\"" + clientUser.getIp() + ":" + clientUser.getPort() + "\"\n");
            }
        });
        buttonDisconnect.setOnMouseClicked(event -> disconnect(true, true));
        buttonRoomDisconnect.setOnMouseClicked(event -> roomDisconnect(false));
        begin();
    }

    public void sendMessage(String message) {
        try {
            clientUser.getClientUserConnection().sendMessage(message);
        } catch (IOException e) {
            printMessage("The server didn't respond. You will get disconnected from the server!");
            disconnect(false, true);
        }
    }

    public void sendUserMessage(String message) {
        sendMessage(clientUser.getUsername() + " -> " + message);
    }

    public void addRoomToList(String room) {
        for (MenuItem menuItem : dropDownRoom.getItems()) {
            if (menuItem.getText().toLowerCase().equals(room.toLowerCase())) {
                return;
            }
        }
        dropDownRoom.getItems().add(new MenuItem(room));
        dropDownRoom.getItems().get(dropDownRoom.getItems().size() - 1).setOnAction(event -> roomConnect(room));
    }

    public void printMessage(String message) {
        textArea.setText(textArea.getText() + message + "\n" );
        textField.setText("");
    }

    public void setClientUser(ClientUser clientUser) {
        this.clientUser = clientUser;
    }

    public boolean isConnected() {
        return connected;
    }

    public void disconnect(boolean serverReachable, boolean complete) {
        if (isConnected()) {
            if (serverReachable) {
                if (inRoom) {
                    roomDisconnect(complete);
                }
            }
            serverDisconnected();
            printMessage("Disconnected from the server\n");
        }
    }

    public void roomConnect(String room) {
        if (inRoom) {
            roomDisconnect(false);
        }
        sendMessage("joinroom " + room + " " + clientUser.getUsername());
        textField.setDisable(false);
    }

    public void roomDisconnect(boolean complete) {
        sendMessage("[~Room~] disconnect " + complete + " " + clientUser.getUsername());
        roomDisconnected();
    }

    public void begin() {
        buttonConnect.setDisable(true);
        buttonDisconnect.setDisable(true);
        buttonSend.setDisable(false);
        buttonRoomDisconnect.setDisable(true);
        textField.setDisable(false);
        dropDownRoom.setDisable(true);
        connected = false;
        printMessage("Hello");
        printMessage("Please type in your username ...");
    }

    public void setUsername(String username) {
        if (username.trim().length() > 0 && username.length() >= 3 && username.length() <= 20) {
            clientUser.setUsername(username);
            roomDisconnected();textField.setOnKeyPressed(event -> {
                if (event.getCode() == KeyCode.ENTER) {
                    sendUserMessage(textField.getText());
                }
            });
            buttonSend.setOnMouseClicked(event -> sendUserMessage(textField.getText()));
            textField.setPromptText("Please enter your text ...");
            buttonConnect.setDisable(false);
            printMessage("That's a good name " + clientUser.getUsername());
            clientUser.getPrimaryStage().setTitle(clientUser.getPrimaryStage().getTitle() + " (" + username + ")");
        } else {
            printMessage("Your username has to be longer than 3, shorter than 20 and not empty!");
        }
        textField.setText("");
        textField.setDisable(true);
    }

    public void serverConnected() {
        buttonConnect.setDisable(true);
        buttonDisconnect.setDisable(false);
        buttonSend.setDisable(false);
        dropDownRoom.setDisable(false);
        connected = true;
    }

    public void serverDisconnected() {
        if (clientUser.getClientUserConnection().getSocket() != null) {
            sendMessage("disconnect");
        }
        buttonConnect.setDisable(false);
        buttonDisconnect.setDisable(true);
        buttonSend.setDisable(true);
        textField.setDisable(true);
        dropDownRoom.getItems().clear();
        dropDownRoom.setDisable(true);
        connected = false;
        textArea.setText("");
    }

    public void roomConnected() {
        buttonRoomDisconnect.setDisable(false);
        textField.setDisable(false);
        inRoom = true;
    }

    public void roomDisconnected() {
        buttonRoomDisconnect.setDisable(true);
        textField.setDisable(true);
        inRoom = false;
    }

    public boolean isInRoom() {
        return inRoom;
    }

}
