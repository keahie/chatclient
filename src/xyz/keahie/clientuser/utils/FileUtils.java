package xyz.keahie.clientuser.utils;

import java.io.*;

public class FileUtils {

    public static int getInteger(File file, String searchString) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            String[] array = line.toLowerCase().split(" ");
            if (searchString.toLowerCase().contains(array[0].substring(array[0].length()))) {
                return Integer.parseInt(line.split(" ")[1]);
            }
        }
        return -1;
    }
}
